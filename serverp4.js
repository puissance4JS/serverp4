var express = require('express');
var app = express();
var server = require('http').Server(app);
var mysql = require('mysql');
var io = require("socket.io")(server);

var bodyParser = require('body-parser');
var urlencodedparser = bodyParser.urlencoded({ extended: false });

app.use(express.static('public'));

app.use((req, res, next) => {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE, OPTIONS");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Authorization, x-access-token, Accept");
	next();
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var bdd;

connectBDD = () => {
	bdd = mysql.createConnection({
		host: "127.0.0.1",
		user: "root",
		password: "masterhozer",
		database: "puissance4"
	});
	bdd.connect((err) => {
		if (err) {
			console.log("can't connect to puissance4 BDD");
			setTimeout(connectBDD, 2000);
		}
	})
	bdd.on('error', (err) => {
		console.log("mysql server reconnected...");
		if (err.code == "PROTOCOL_CONNECTION_LOST")
			connectBDD();
		else {
			throw err;
			console.log("mysql server can't reconnect")
		}
	})
}

connectBDD();

server.listen(8082, () => {
    var host = server.address().address;
    var port = server.address().port;
    console.log("Puissance4 Server is running on: 51.77.137.116:8082");
})

app.get("/", (err, res) => {
	res.sendFile(__dirname + "/welcome.html");
})


io.on("connection", (socket) => {
	console.log("new user connected, socket.id: " + socket.id);

	socket.on("connected", (data) => {
		console.log("USER " + data.user_id + " SOCKET: " + socket.id + " IS CONNECTED");
	});

	socket.on("disconnect", () => {
		console.log("user is deconnected, socket.id: " + socket.id);
	});

	socket.on("new user", (data) => {
		console.log("SOCKET new user");
		console.log(data);
		bdd.query("SELECT email FROM user WHERE email = \'" + data.email + "\'", (err, rows) => {
			if (err) throw err;
			if (rows.length != 0) {
				console.log(rows[0].email + " is connected");
				console.log("data.email = " + data.email);
				bdd.query("UPDATE user SET ? WHERE email = \'" + data.email + "\'", {socket: socket.id}, (err, result) => {
					if (err) throw err;
				});
			}
		});
	});

	socket.on("play", (data) => {
		console.log("SOCKET play");
		console.log(data);
		bdd.query("SELECT user_id1, user_id2, game_id FROM room WHERE game_name = \'" + data.game_name + "\'", (err, rows) => {
			if (rows[0].user_id1 == data.user_id) {
				place_pawn(1, rows[0].game_id, data.col+1, data.game_name);
			}
			else if (rows[0].user_id2 == data.user_id) {
				place_pawn(2, rows[0].game_id, data.col+1, data.game_name);
			}
		})
	})
})

send_board = (game_name) => {
		bdd.query("SELECT col1, col2, col3, col4, col5, col6, col7 FROM game WHERE game_id = (SELECT game_id FROM room WHERE game_name = \'" + game_name + "\')", (err, rows) => {
		if (err) throw err;
		var data = rows;
		bdd.query("SELECT socket FROM user WHERE user_id = (SELECT user_id1 FROM room WHERE game_name = \'" + game_name + "\') UNION ALL " +
				"SELECT socket FROM user WHERE user_id = (SELECT user_id2 FROM room WHERE game_name = \'" + game_name + "\')", (err, rows) => {
				if (err) throw err;
				console.log(data);
				io.sockets.to(rows[0].socket).emit("play", data);
				io.sockets.to(rows[1].socket).emit("play", data);
		})
	})
}

check_victory = (player, game_id, col, line) => {
	bdd.query("SELECT * FROM game WHERE game_id = " + game_id, (err, rows) => {
		if (err) throw err;
		console.log();
	})
}

place_pawn = (player, game_id, col, game_name) => {
	bdd.query("SELECT * FROM game WHERE game_id = " + game_id, (err, rows) => {
		if (err) throw err;
		var out = 0;
		for (var i = 0; i < rows.length && out === 0; i++) {
			var j = i + 1;
			switch (col) {
				case 1:
					if (rows[i].col1 === 0) {
						bdd.query("UPDATE game SET ? WHERE game_id = " + game_id + " AND line = " + j, {col1: player} , (err, result) => {
							if (err) throw err;
							send_board(game_name);
						})
						out = 1;
					}
					break;
				case 2:
					if (rows[i].col2 === 0) {
						bdd.query("UPDATE game SET ? WHERE game_id = " + game_id + " AND line = " + j, {col2: player} , (err, result) => {
							if (err) throw err;
							send_board(game_name);
						})
						out = 1;
					}
					break;
				case 3:
					if (rows[i].col3 === 0) {
						bdd.query("UPDATE game SET ? WHERE game_id = " + game_id + " AND line = " + j, {col3: player} , (err, result) => {
							if (err) throw err;							
							send_board(game_name);
						})
						out = 1;
					}
					break;
				case 4:
					if (rows[i].col4 === 0) {
						bdd.query("UPDATE game SET ? WHERE game_id = " + game_id + " AND line = " + j, {col4: player} , (err, result) => {
							if (err) throw err;
							send_board(game_name);
						})
						out = 1;
					}
					break;
				case 5:
					if (rows[i].col5 === 0) {
						bdd.query("UPDATE game SET ? WHERE game_id = " + game_id + " AND line = " + j, {col5: player} , (err, result) => {
							if (err) throw err;
							send_board(game_name);
						})
						out = 1;
					}
					break;
				case 6:
					if (rows[i].col6 === 0) {
						bdd.query("UPDATE game SET ? WHERE game_id = " + game_id + " AND line = " + j, {col6: player} , (err, result) => {
							if (err) throw err;
							send_board(game_name);
						})
						out = 1;
					}
					break;
				case 7:
					if (rows[i].col7 === 0) {
						bdd.query("UPDATE game SET ? WHERE game_id = " + game_id + " AND line = " + j, {col7: player} , (err, result) => {
							if (err) throw err;
							send_board(game_name);
						})
						out = 1;
					}
					break;
				default:
					break;
			}
		}
	})
	console.log("colonne: " + col);
}



app.post("/inscription", urlencodedparser, (req, res) => {
	console.log("inscription");
	if (!req.body.user_name || !req.body.email || !req.body.password)
		res.send(JSON.stringify({data: {status: "bad param", code: 400, format: "JSON"}}));
	else {
		bdd.query("SELECT email FROM user WHERE email = \'" + req.body.email + "\'", (err, rows) => {
			if (err) throw err;
			if (rows.length != 0)
				res.send(JSON.stringify({data: {status: "email already taken", code: 401, format: "JSON"}}));
			else {
				bdd.query("INSERT INTO user SET ?", {user_name:req.body.user_name, email:req.body.email, password:req.body.password, win:0, defeat:0}, (err, result) => {
					if (err) throw err;
				})
				console.log(req.body.email + " is registered");
				res.send(JSON.stringify({data: {status: "ok", code: 200, format: "JSON"}}));
			}
		})
	}
})

app.post("/connexion", urlencodedparser, (req, res) => {
	console.log("connexion");
	if (!req.body.email || !req.body.password)
		res.send(JSON.stringify({data: {status: "bad param", code: 400, format: "JSON"}}));
	else {
		bdd.query("SELECT * FROM user WHERE email = \'" + req.body.email + "\'", (err, rows) => {
			if (err) throw err;
			if (rows.length == 0)
				res.send(JSON.stringify({data: {status: "email not found", code: 404, format: "JSON"}}));
			else {
				if (rows[0].password === req.body.password) {
					let user = {user_id: rows[0].user_id, email: rows[0].email, win: rows[0].win, defeat: rows[0].defeat};
					res.send(JSON.stringify({data: {status: "ok", code: 200, format: "JSON"}, user}));
					console.log(req.body.email + " is connected");					
				}
				else
					res.send(JSON.stringify({data: {status: "bad password", code: 401, format: "JSON"}}));
			}
		})
	}
})

app.post("/room/create", urlencodedparser, (req, res) => {
	console.log("/room/create");
	if (!req.body.email || !req.body.game_name)
		res.send(JSON.stringify({data: {status: "bad param", code: 400, format: "JSON"}}));
	else {
		bdd.query("SELECT email FROM user WHERE email = \'" + req.body.email + "\'", (err, rows) => {
			if (err) throw err;
			if (rows.length == 0)
				res.send(JSON.stringify({data: {status: "email not find", code: 404, format: "JSON"}}));
			else {
				bdd.query("SELECT game_name FROM room WHERE game_name = \'" + req.body.game_name + "\'", (err, rows) => {
					if (err) throw err;
					if (rows.length != 0)
						res.send(JSON.stringify({data: {status: "game name already taken", code: 401, format: "JSON"}}));
					else {
						bdd.query("SELECT user_id FROM user WHERE email = \'" + req.body.email + "\'", (err, rows) => {
							if (err) throw err;
							bdd.query("INSERT INTO room SET ?", {game_name:req.body.game_name, game_status:1, user_id1:rows[0].user_id, user_id2:0}, (err, result) => {
								if (err) throw err;
							})
							console.log(req.body.email + " has created room " + req.body.game_name);
							res.send(JSON.stringify({data: {status: "ok", code: 200, format: "JSON"}}));
						})
					}
				})
			}

		})
	}
})

app.post("/room/join", urlencodedparser, (req, res) => {
	console.log("/room/join");
	if (!req.body.email || !req.body.game_name)
		res.send(JSON.stringify({data: {status: "bad param", code: 400, format: "JSON"}}));
	else {
		bdd.query("SELECT email FROM user WHERE email = \'" + req.body.email + "\'", (err, rows) => {
			if (err) throw err;
			if (rows.length == 0)
				res.send(JSON.stringify({data: {status: "email not find", code: 404, format: "JSON"}}));
			else {
				bdd.query("SELECT game_name, game_status FROM room WHERE game_name = \'" + req.body.game_name + "\'", (err, rows) => {
					if (err) throw err;
					else if (rows.length == 0)
						res.send(JSON.stringify({data: {status: "game not found", code: 404, format: "JSON"}}));
					else if (rows[0].game_status != 1)
						res.send(JSON.stringify({data: {status: "game is full", code: 401, format: "JSON"}}));
					else {
						bdd.query("SELECT user_id FROM user WHERE email = \'" + req.body.email + "\'", (err, rows) => {
							if (err) throw err;
							bdd.query("UPDATE room SET ? WHERE game_name = \'" + req.body.game_name + "\'", {user_id2:rows[0].user_id, game_status:2}, (err, result) => {
								if (err) throw err;
							})
						})
						bdd.query("SELECT user.socket FROM user JOIN room WHERE user.user_id = room.user_id1 AND room.game_name = \'" + req.body.game_name + "\'", (err, rows) => {
							if (err) throw err;
							console.log(rows);
							let data = {email: req.body.email};
							io.sockets.to(rows[0].socket).emit("join", data);
						})
						console.log(req.body.email + " has joined room " + req.body.game_name);
						res.send(JSON.stringify({data: {status: "ok", code: 200, format: "JSON"}}));
					}
				})
			}
		})
	}
})

app.post("/room/leave", urlencodedparser, (req, res) => {
	console.log("/room/leave");
	if (!req.body.email || !req.body.game_name)
		res.send(JSON.stringify({data: {status: "bad param", code: 400, format: "JSON"}}));
	else {
		bdd.query("SELECT email FROM user WHERE email = \'" + req.body.email + "\'", (err, rows) => {
			if (err) throw err;
			if (rows.length == 0)
				res.send(JSON.stringify({data: {status: "email not find", code: 404, format: "JSON"}}));
			else {
				bdd.query("SELECT game_id, game_status FROM room WHERE game_name = \'" + req.body.game_name + "\'", (err, rows) => {
					if (err) throw err;
					if (rows.length == 0)
						res.send(JSON.stringify({data: {status: "room doesn't exit", code: 404, format: "JSON"}}));
					else {
						if (rows[0].game_status == 1) {
							bdd.query("DELETE FROM game WHERE game_id = (SELECT game_id FROM room WHERE game_name = \'" + req.body.game_name + "\')", (err, result) => {
								if (err) throw err;
							})
							bdd.query("DELETE FROM room WHERE game_name = \'" + req.body.game_name + "\'", (err, result) => {
								if (err) throw err;
							})
						console.log("everyone has leaved");
						}
						else {
							bdd.query("SELECT user_id FROM user WHERE email = \'" + req.body.email + "\' UNION ALL " +
									"SELECT user_id1 FROM room WHERE game_name = \'" + req.body.game_name + "\' UNION ALL " +
									"SELECT user_id2 FROM room WHERE game_name = \'" + req.body.game_name + "\'", (err, rows) => {
								if (err) throw err;
								if (rows[0].user_id == rows[1].user_id)
									bdd.query("UPDATE room SET ? WHERE game_name = \'" + req.body.game_name + "\'", {game_status:1, user_id1:rows[2].user_id, user_id2:0}, (err, result) => {
										if (err) throw err;
										console.log("master has leaved");
									})
								else if (rows[0].user_id == rows[2].user_id)
									bdd.query("UPDATE room SET ? WHERE game_name = \'" + req.body.game_name + "\'", {game_status:1, user_id2:0}, (err, result) => {
										if (err) throw err;
										console.log("chall has leaved");
									})
								let data = {email: req.body.email};
								io.sockets.to(rows[0].socket).emit("leave", data);
							})
						}
						res.send(JSON.stringify({data: {status: "ok", code: 200, format: "JSON"}}));
					}
				})
			}
		})
	}
})

app.post("/room/get", urlencodedparser, (req, res) => {
	console.log("/room/get");
	if (!req.body.email)
		res.send(JSON.stringify({data: {status: "bad param", code: 400, format: "JSON"}}));
	else {
		bdd.query("SELECT game_name, game_status FROM room", (err, result) => {
			if (err) throw err;
			res.send(JSON.stringify({data: {status: "ok", code: 200, format: "JSON"}, result}));
		})
	}
})

app.post("/game/start", urlencodedparser, (req, res) => {
	console.log("/game/start");
	if (!req.body.game_name)
		res.send(JSON.stringify({data: {status: "bad param", code: 400, format: "JSON"}}));
	else {
		bdd.query("SELECT game_name FROM room WHERE game_name = \'" + req.body.game_name + "\'", (err, rows) => {
			if (err) throw err;
			if (rows.length == 0)
				res.send(JSON.stringify({data: {status: "game not found", code: 404, format: "JSON"}}));
			else {
				bdd.query("SELECT * FROM game WHERE game_id = (SELECT game_id FROM room WHERE game_name = \'" + req.body.game_name + "\')", (err, rows) => {
					if (err) throw err;
					if (rows.length == 0) {
						bdd.query("SELECT game_id FROM room WHERE game_name = \'" + req.body.game_name + "\'", (err, rows) => {
							if (err) throw err;
							bdd.query("INSERT INTO game SET ?", {game_id: rows[0].game_id, line: 1, col1: 0, col2: 0, col3: 0, col4: 0, col5: 0, col6: 0, col7: 0}, (err, result) => {
								if (err) throw err;
							})
							bdd.query("INSERT INTO game SET ?", {game_id: rows[0].game_id, line: 2, col1: 0, col2: 0, col3: 0, col4: 0, col5: 0, col6: 0, col7: 0}, (err, result) => {
								if (err) throw err;
							})
							bdd.query("INSERT INTO game SET ?", {game_id: rows[0].game_id, line: 3, col1: 0, col2: 0, col3: 0, col4: 0, col5: 0, col6: 0, col7: 0}, (err, result) => {
								if (err) throw err;
							})
							bdd.query("INSERT INTO game SET ?", {game_id: rows[0].game_id, line: 4, col1: 0, col2: 0, col3: 0, col4: 0, col5: 0, col6: 0, col7: 0}, (err, result) => {
								if (err) throw err;
							})
							bdd.query("INSERT INTO game SET ?", {game_id: rows[0].game_id, line: 5, col1: 0, col2: 0, col3: 0, col4: 0, col5: 0, col6: 0, col7: 0}, (err, result) => {
								if (err) throw err;
							})
							bdd.query("INSERT INTO game SET ?", {game_id: rows[0].game_id, line: 6, col1: 0, col2: 0, col3: 0, col4: 0, col5: 0, col6: 0, col7: 0}, (err, result) => {
								if (err) throw err;
							})
						})
						bdd.query("UPDATE room SET ? WHERE game_name = \'" + req.body.game_name + "\'", {game_status: 3}, (err, result) => {
							if (err) throw err;
						})
						res.send(JSON.stringify({data: {status: "ok", code: 200, format: "JSON"}}));
						bdd.query("SELECT socket FROM user WHERE user_id = (SELECT user_id1 FROM room WHERE game_name = \'" + req.body.game_name + "\') UNION ALL " +
							"SELECT socket FROM user WHERE user_id = (SELECT user_id2 FROM room WHERE game_name = \'" + req.body.game_name + "\')", (err, rows) => {
								if (err) throw err;
								let data = {};
								io.sockets.to(rows[0].socket).emit("start", data);
								io.sockets.to(rows[1].socket).emit("start", data);
						})
					}
					else {
						res.send(JSON.stringify({data: {status: "game alredy exists", code: 401, format: "JSON"}}));
					}
				})
			}
		})
	}
})
